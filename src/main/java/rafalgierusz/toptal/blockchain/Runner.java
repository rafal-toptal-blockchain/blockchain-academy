package rafalgierusz.toptal.blockchain;

import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;

import rafalgierusz.toptal.blockchain.block.Block;
import rafalgierusz.toptal.blockchain.chain.Chain;
import rafalgierusz.toptal.blockchain.common.Payload;
import rafalgierusz.toptal.blockchain.transaction.Transaction;

/**
 * Transaction flow:
 * <ul>
 * <li>request for unsigned transaction</li>
 * <li>sign it on your side</li>
 * <li>submit signed transaction</li>
 * </ul>
 */

@RestController
@SpringBootApplication
public class Runner {
	private static final Logger LOGGER = LoggerFactory.getLogger(Runner.class);

	@Autowired
	private Chain chain;

	public Runner() {
		Timer t = new Timer();
		t.scheduleAtFixedRate(new TimerTask() {
			public void run() {
				LOGGER.info("Chain: {}: {}", chain.validate(),
						chain.getBlocks().stream().map(Block::getHash).collect(Collectors.joining(",")));
			}
		}, 1000, 5000);
	}

	// *****
	// utils
	// *****

	@RequestMapping(path = "/diffuculty-target", produces = "application/json; charset=UTF-8")
	public BigInteger getDifficultyTarget() throws JsonProcessingException {
		return chain.getDifficultyTarget();
	}

	// *****
	// block
	// *****

	@RequestMapping(path = "/block-count", produces = "application/json; charset=UTF-8")
	public int getBlockCount() {
		return chain.getBlockCount();
	}

	@RequestMapping(path = "/block-current", produces = "application/json; charset=UTF-8")
	public Block getCurrentBlock() {
		return chain.getCurrentBlock();
	}

	@RequestMapping(path = "/block/{hash}", produces = "application/json; charset=UTF-8")
	public Block getBlock(@PathVariable String hash) {
		return chain.getBlock(hash);
	}

	@RequestMapping(path = "/block/add")
	public void addBlock(@RequestBody Block block) {
		chain.add(block);
	}

	// ***********
	// transaction
	// ***********

	@RequestMapping(path = "/transaction/{hash}", produces = "application/json; charset=UTF-8")
	public Transaction getTransaction(@PathVariable String hash) {
		return chain.getTransaction(hash);
	}

	@RequestMapping(path = "/transaction/create/{sender}/{recipient}/{amount}", method = RequestMethod.POST, consumes = "application/json; charset=UTF-8", produces = "application/json; charset=UTF-8")
	public Transaction createTransaction(@PathVariable String sender, @PathVariable String recipient,
			@PathVariable BigInteger amount, @RequestBody Payload payload) {
		return chain.crateTransaction(sender, recipient, amount, StringUtils.isEmpty(payload) ? null : payload);
	}

	@RequestMapping(path = "/transaction/submit", method = RequestMethod.POST, consumes = "application/json; charset=UTF-8", produces = "application/json; charset=UTF-8")
	public boolean submitTransaction(@RequestBody Transaction transaction) throws IOException {
		return chain.addUnprocessedTransaction(transaction);
	}

	@RequestMapping(path = "/transaction/published", produces = "application/json; charset=UTF-8")
	public List<Transaction> getUnprocessedTransactions() throws IOException {
		return new ArrayList<>(chain.getPublishedTransactions().values());
	}

	// ******
	// wallet
	// ******

	@RequestMapping(path = "/balance/{publicKey}", produces = "application/json; charset=UTF-8")
	public BigInteger getBalance(@PathVariable String publicKey) {
		return chain.getBalance(publicKey);
	}

	// *****
	// nodes
	// *****

	@RequestMapping(path = "/node-block-check", method = RequestMethod.POST, consumes = "application/json; charset=UTF-8", produces = "application/json; charset=UTF-8")
	public boolean checkBlockFromNode(@RequestBody Block block) {
		try {
			LOGGER.info("Received block from other node: {}", block.getHash());

			boolean result = chain.processBlockFromOtherNode(block);

			if (result) {
				LOGGER.info("Other node block accepted: {}", block.getHash());
			} else {
				LOGGER.info("Other node block rejected, sync performed: {}", block.getHash());
			}

			return result;
		} catch (Exception e) {
			LOGGER.info("Other node block rejected (case 2), sync performed: {}", block.getHash());

			return false;
		}
	}

	// *********************
	// enable only for tests
	// *********************

	@RequestMapping(path = "publish")
	public void publish() {
		chain.publishUnprocessedTransactions();
	}

	@RequestMapping(path = "is-valid", produces = "application/json; charset=UTF-8")
	public boolean isValid() {
		return chain.validate();
	}

	public static void main(String[] args) throws IOException {
		SpringApplication.run(Runner.class, args);
	}
}
