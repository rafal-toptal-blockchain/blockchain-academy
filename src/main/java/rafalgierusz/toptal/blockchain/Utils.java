package rafalgierusz.toptal.blockchain;

import static java.nio.charset.StandardCharsets.UTF_8;

import java.math.BigInteger;
import java.security.DigestException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.ECGenParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.List;
import java.util.stream.Collectors;

import com.google.common.io.BaseEncoding;

import rafalgierusz.toptal.blockchain.block.Block;
import rafalgierusz.toptal.blockchain.transaction.Transaction;

public class Utils {
	private static final BigInteger NBITS_BASE = BigInteger.valueOf(256);

	// https://blog.ethereum.org/2015/07/05/on-abstraction
	private static final AlgorithmParameterSpec ALGORITHM_PARAM_SPEC = new ECGenParameterSpec("secp256k1");
	private static final String SECURITY_PROVIDER = "SunEC";
	private static final String KEY_PAIR_ALGORITHM = "EC";
	private static final String SIGNATURE_ALGORITHM = "SHA512withECDSA";
	private static final SecureRandom SECURE_RANDOM;

	/**
	 * Not to be used directly, see {@link #getMessageDigest()}
	 */
	private static final MessageDigest MESSAGE_DIGEST;

	static {
		try {
			// https://stackoverflow.com/a/27638413
			SECURE_RANDOM = SecureRandom.getInstance("SHA1PRNG");
			MESSAGE_DIGEST = MessageDigest.getInstance("SHA-256");
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}

	// https://bitcoin.org/en/developer-reference#target-nbits
	public static BigInteger nBitsToBigInteger(int nBits) {
		int mantissa = nBits & 0x00ffffff;
		int exponent = nBits >> 24;

		return NBITS_BASE.pow(exponent - 3).multiply(BigInteger.valueOf(mantissa));
	}

	public static String hash(String content) {
		return BaseEncoding.base16().lowerCase().encode(hash(content, false));
	}

	public static byte[] hash(String content, boolean withLeadingZeroByte) {
		MessageDigest messageDigest = getMessageDigest();
		messageDigest.update(content.getBytes(UTF_8));

		if (withLeadingZeroByte) {
			byte[] signature = new byte[33];
			signature[0] = 0x00;

			try {
				messageDigest.digest(signature, 1, 32);
			} catch (DigestException e) {
				throw new RuntimeException(e);
			}

			return signature;
		} else {
			return messageDigest.digest();
		}
	}

	private static MessageDigest getMessageDigest() {
		synchronized (MESSAGE_DIGEST) {
			try {
				return (MessageDigest) MESSAGE_DIGEST.clone();
			} catch (CloneNotSupportedException e) {
				throw new RuntimeException(e);
			}
		}
	}

	public static boolean checkDifficulty(Block block, BigInteger difficultyTarget) {
		return new BigInteger("00" + block.getHash(), 16).compareTo(difficultyTarget) == -1;
	}

	public static KeyPair createKeyPair() {
		try {
			KeyPairGenerator keyGen = KeyPairGenerator.getInstance(KEY_PAIR_ALGORITHM, SECURITY_PROVIDER);
			keyGen.initialize(ALGORITHM_PARAM_SPEC, SECURE_RANDOM);

			return keyGen.generateKeyPair();
		} catch (NoSuchAlgorithmException | InvalidAlgorithmParameterException | NoSuchProviderException e) {
			throw new RuntimeException(e);
		}
	}

	public static String sign(PrivateKey key, String content) {
		try {
			Signature signature = Signature.getInstance(SIGNATURE_ALGORITHM, SECURITY_PROVIDER);
			signature.initSign(key);
			signature.update(content.getBytes(UTF_8));
			return BaseEncoding.base16().lowerCase().encode(signature.sign());
		} catch (NoSuchAlgorithmException | InvalidKeyException | SignatureException | NoSuchProviderException e) {
			throw new RuntimeException(e);
		}
	}

	public static boolean verifySignature(PublicKey key, String content, String signatureHex) {
		try {
			Signature signature = Signature.getInstance(SIGNATURE_ALGORITHM, SECURITY_PROVIDER);
			signature.initVerify(key);
			signature.update(content.getBytes(UTF_8));
			return signature.verify(BaseEncoding.base16().lowerCase().decode(signatureHex));
		} catch (NoSuchAlgorithmException | InvalidKeyException | SignatureException | NoSuchProviderException e) {
			throw new RuntimeException(e);
		}
	}

	public static String serializeKey(Key key) {
		return BaseEncoding.base16().lowerCase().encode(key.getEncoded());
	}

	public static PublicKey deserializePublicKey(String key) {
		try {
			return KeyFactory.getInstance(KEY_PAIR_ALGORITHM, SECURITY_PROVIDER)
					.generatePublic(new X509EncodedKeySpec(BaseEncoding.base16().lowerCase().decode(key)));
		} catch (InvalidKeySpecException | NoSuchAlgorithmException | NoSuchProviderException e) {
			throw new IllegalArgumentException("Incorrect public key");
		}
	}

	public static PrivateKey deserializePrivateKey(String key) {
		try {
			return KeyFactory.getInstance(KEY_PAIR_ALGORITHM, SECURITY_PROVIDER)
					.generatePrivate(new PKCS8EncodedKeySpec(BaseEncoding.base16().lowerCase().decode(key)));
		} catch (InvalidKeySpecException | NoSuchAlgorithmException | NoSuchProviderException e) {
			throw new IllegalArgumentException("Incorrect private key");
		}
	}

	public static String calculateMerkleRoot(List<Transaction> transactions) {
		if (transactions.isEmpty()) {
			return null;
		}

		List<String> layer = transactions.stream().map(Transaction::getHash).collect(Collectors.toList());

		while (layer.size() > 1) {
			for (int i = 0; i < layer.size() - 1; i++) {
				layer.set(i, hash(layer.get(i) + layer.get(i + 1)));
			}

			layer.remove(layer.size() - 1);
		}

		return layer.get(0);
	}
}
