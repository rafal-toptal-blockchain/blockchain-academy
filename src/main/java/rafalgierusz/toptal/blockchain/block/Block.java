package rafalgierusz.toptal.blockchain.block;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;
import static rafalgierusz.toptal.blockchain.Utils.calculateMerkleRoot;
import static rafalgierusz.toptal.blockchain.Utils.checkDifficulty;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import rafalgierusz.toptal.blockchain.common.Hashable;
import rafalgierusz.toptal.blockchain.common.Payload;
import rafalgierusz.toptal.blockchain.common.Signable;
import rafalgierusz.toptal.blockchain.transaction.Transaction;
import rafalgierusz.toptal.blockchain.wallet.Wallet;

@JsonInclude(NON_NULL)
public class Block extends Signable<Block> implements Hashable {
	private String previousHash;
	private long timestamp;

	private int nonce;

	protected Payload payload;

	protected List<Transaction> transactions = new ArrayList<>();

	private String hash;

	public Block() {
	}

	/**
	 * Introduced to be used only by Genesis Block
	 */
	protected Block(String hash, long timestamp, int nonce, Transaction transaction) {
		this.hash = hash;
		this.timestamp = timestamp;
		this.nonce = nonce;

		this.previousHash = null;

		transactions.add(transaction);
	}

	public Block(String previousHash) {
		this.previousHash = previousHash;
		this.timestamp = System.currentTimeMillis();

		this.hash = calculateHash(); // correct but not mined
	}

	public void addTransaction(Transaction transaction) {
		if (transaction == null || !transaction.verify()) {
			throw new IllegalArgumentException("incorrect transaction");
		}

		transactions.add(transaction);
	}

	public String getPreviousHash() {
		return previousHash;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public String getHash() {
		return hash;
	}

	public Payload getPayload() {
		return payload;
	}

	public int getNonce() {
		return nonce;
	}

	public List<Transaction> getTransactions() {
		return transactions;
	}

	public Block mine(Wallet minerWallet, Payload payload, BigInteger difficultyTarget) {
		if (minerWallet == null || minerWallet.getPrivateKey() == null || minerWallet.getPublicKey() == null) {
			throw new IllegalArgumentException("Incorrect wallet");
		}

		if (payload != null && (!payload.verifySignature()
				|| !payload.getPublicKey().equals(minerWallet.getPublicKeySerialized()))) {
			throw new IllegalArgumentException("Incorrect payload");
		}

		this.payload = payload;

		while (!checkDifficulty(this, difficultyTarget)) {
			nonce++;
			hash = calculateHash();
		}

		return sign(minerWallet);
	}

	@Override
	public String prepareContentToHash() {
		StringBuilder builder = new StringBuilder(String.valueOf(previousHash)).append(",").append(timestamp)
				.append(",").append(nonce);

		if (!transactions.isEmpty()) {
			builder.append(",").append(calculateMerkleRoot(transactions));
		}

		if (payload != null) {
			builder.append(",").append(payload.getSignature());
		}

		return builder.toString();
	}

	@Override
	protected String prepareContentToSign() {
		return hash + "," + prepareContentToHash();
	}

	/**
	 * Checked: previousHash, hash, signature, payload, difficulty and transactions
	 */
	public boolean verify(String previousHash, BigInteger difficultyTarget) {
		boolean isPreviousHashCorrect = previousHash == this.previousHash
				|| (previousHash != null && previousHash.equals(previousHash));
		boolean isPayloadCorrect = payload == null
				|| (payload.verifySignature() && payload.getPublicKey().equals(getPublicKey()));

		return isPreviousHashCorrect && verifyHash() && verifySignature() && isPayloadCorrect
				&& checkDifficulty(this, difficultyTarget) && getTransactions().size() > 0
				&& getTransactions().stream().allMatch(Transaction::verify);
	}
}
