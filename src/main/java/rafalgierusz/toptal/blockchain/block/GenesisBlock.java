package rafalgierusz.toptal.blockchain.block;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

import rafalgierusz.toptal.blockchain.common.Payload;
import rafalgierusz.toptal.blockchain.transaction.GenesisTransacion;
import rafalgierusz.toptal.blockchain.wallet.Wallet;
import rafalgierusz.toptal.blockchain.wallet.WalletMiner1;
import rafalgierusz.toptal.blockchain.wallet.WalletMiner2;
import rafalgierusz.toptal.blockchain.wallet.WalletMiner3;
import rafalgierusz.toptal.blockchain.wallet.WalletMiner4;
import rafalgierusz.toptal.blockchain.wallet.WalletMiner5;

public class GenesisBlock extends Block {
	public static String PAYLOAD_KEY_MINERS = "miners";

	public GenesisBlock() {
		super("0000e2b1050bc20dd4ad63c96099e2068ab7be2808bed488d3b674adeb897c95", 1523197984215L, 20314,
				new GenesisTransacion());

		Wallet walletMiner1 = new WalletMiner1();
		Wallet walletMiner2 = new WalletMiner2();
		Wallet walletMiner3 = new WalletMiner3();
		Wallet walletMiner4 = new WalletMiner4();
		Wallet walletMiner5 = new WalletMiner5();

		payload = new Payload(Map.of( //
				"message", "Genesis", //
				PAYLOAD_KEY_MINERS, Arrays.asList(walletMiner1, walletMiner2, walletMiner3, walletMiner4, walletMiner5)
						.stream().map(Wallet::getPublicKeySerialized).collect(Collectors.joining(",")) //
		));
		payload.setPublicKey(walletMiner1.getPublicKeySerialized());
		payload.setSignature(
				"304302206eeceed905d3da35d01d372692dfaed43f62b4cbf3e9ecfa88c567b558f94b74021f5d55bb79ae125919ef462d67b0f43523a56a67a3e00cd00ea309e83c866aee");

		setPublicKey(walletMiner1.getPublicKeySerialized());
		setSignature(
				"304402202ea743f04ebd3a9ea00cd89b68eb2bdac3c2e652651835bed805c272e47dd4610220678d0a7ba6c42bb9db977f48d7269db60f6097768b19c799aeeba28c48dabb4b");
	}

	@Override
	public Block mine(Wallet minerWallet, Payload payload, BigInteger difficultyTarget) {
		throw new UnsupportedOperationException("Genesis block cannot be mined");
	}

//	 public static void main(String[] args) throws
//	 com.fasterxml.jackson.core.JsonProcessingException {
//	 GenesisBlock g = new GenesisBlock();
//	
//	 Payload payload = new Payload(Map.of( //
//	 "message", "Genesis", //
//	 PAYLOAD_KEY_MINERS,
//	
//	 "3056301006072a8648ce3d020106052b8104000a03420004dd59a6c7e97d15f1ced3697135780278fb165c63cc4158b13bd9d19d2b0a4bb195c2963cde74b7ee3db7e88856adb14cb9775df6f6c52e690760a9e684d08fd1,"
//	 +
//	
//	 "3056301006072a8648ce3d020106052b8104000a0342000402cdd2e7db217dc2f6a5df2b1c3286620fef409253157c0761cf499f130974a5cecb848138b3bd654bfe26f157d81695d1bcaa4d593ce0dbb94fa27697bdaa83,"
//	 +
//	
//	 "3056301006072a8648ce3d020106052b8104000a034200048310a5f37e008b66368c4a97ba8c36ffb1ccb89ac0e3c84b31ce8644008349b17ec1ff780878b366c6fcae8475215419907a34910b07213799259e88d2b4a177,"
//	 +
//	
//	 "3056301006072a8648ce3d020106052b8104000a03420004350b844962027b700053aaa3ab8d1d6061c936aaa2e03deda70c1807efeac0d1403b6bb4ec699ff7377e481dbaf6a909388910bf67997501e4f7e43ff2352471,"
//	 +
//	
//	 "3056301006072a8648ce3d020106052b8104000a03420004e281dd894cdd55806a3f0caf30d7d60a7a47bb46132a415c2b44e0d9790115a25f60e10d36fc52b14e0cf620d5d83439e54bbc8a93f6d0b09dde423fb6b6bae5"
//	 //
//	 ));
//	 String prk =
//	 "303e020100301006072a8648ce3d020106052b8104000a04273025020101042068975fdfcba8a26049046b64c69738ad7d690a9f56adfb02f99c104e29ee36b1";
//	 String puk =
//	 "3056301006072a8648ce3d020106052b8104000a03420004dd59a6c7e97d15f1ced3697135780278fb165c63cc4158b13bd9d19d2b0a4bb195c2963cde74b7ee3db7e88856adb14cb9775df6f6c52e690760a9e684d08fd1";
//	
//	 payload.sign(new Wallet(prk, puk));
//	
//	 String transactionPrivateKey =
//	 "303e020100301006072a8648ce3d020106052b8104000a042730250201010420388dfb61229e840b898ece218346e674415d7299df7d7c4700b95ec2876975da";
//	 String transactionPublicKey =
//	 "3056301006072a8648ce3d020106052b8104000a0342000412b5b803dcb6f122a80f60dbd95239ec684da8864ce6c746e8683fe68eb382ea6142a1bd30379bfc3ba3374aefb5f31b851b0556e12f201ca4c5b1321c0102b5";
//	 g.transactions.add(new GenesisTransacion(transactionPrivateKey));
//	
//	 g.mine(new Wallet(prk, puk), payload, nBitsToBigInteger(0x1f00ffff));
//	 System.out.println(new
//	
//	 com.fasterxml.jackson.databind.ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(g));
	
//	 GenesisBlock g = new GenesisBlock();
//	 System.out.println(g.verify(null, nBitsToBigInteger(0x1f00ffff)));
//	 System.out.println(new
//	 com.fasterxml.jackson.databind.ObjectMapper().writerWithDefaultPrettyPrinter()
//	 .writeValueAsString(g));
//	 }
}

// private:303e020100301006072a8648ce3d020106052b8104000a04273025020101042068975fdfcba8a26049046b64c69738ad7d690a9f56adfb02f99c104e29ee36b1
// public:3056301006072a8648ce3d020106052b8104000a03420004dd59a6c7e97d15f1ced3697135780278fb165c63cc4158b13bd9d19d2b0a4bb195c2963cde74b7ee3db7e88856adb14cb9775df6f6c52e690760a9e684d08fd1
//
// private:303e020100301006072a8648ce3d020106052b8104000a0427302502010104202a7f839af1ab3fd274066419e669f8c2e0b299af6eac0df682e7f4a31e4905ef
// public:3056301006072a8648ce3d020106052b8104000a0342000402cdd2e7db217dc2f6a5df2b1c3286620fef409253157c0761cf499f130974a5cecb848138b3bd654bfe26f157d81695d1bcaa4d593ce0dbb94fa27697bdaa83
//
// private:303e020100301006072a8648ce3d020106052b8104000a042730250201010420200e26691066ba4798560467dea830699447053d54b5e87b410a479ce6a84a08
// public:3056301006072a8648ce3d020106052b8104000a034200048310a5f37e008b66368c4a97ba8c36ffb1ccb89ac0e3c84b31ce8644008349b17ec1ff780878b366c6fcae8475215419907a34910b07213799259e88d2b4a177
//
// private:303e020100301006072a8648ce3d020106052b8104000a042730250201010420389872889cdb400751128f2160ac0abacaa29013b625c5a205b08df270673fe7
// public:3056301006072a8648ce3d020106052b8104000a03420004350b844962027b700053aaa3ab8d1d6061c936aaa2e03deda70c1807efeac0d1403b6bb4ec699ff7377e481dbaf6a909388910bf67997501e4f7e43ff2352471
//
// private:303e020100301006072a8648ce3d020106052b8104000a042730250201010420ece381afa3bd8201f35b4deadf83507f42d5cfc550374782b4f52ba3c8092461
// public:3056301006072a8648ce3d020106052b8104000a03420004e281dd894cdd55806a3f0caf30d7d60a7a47bb46132a415c2b44e0d9790115a25f60e10d36fc52b14e0cf620d5d83439e54bbc8a93f6d0b09dde423fb6b6bae5