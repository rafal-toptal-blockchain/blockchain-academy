package rafalgierusz.toptal.blockchain.chain;

import static rafalgierusz.toptal.blockchain.Utils.nBitsToBigInteger;
import static rafalgierusz.toptal.blockchain.block.GenesisBlock.PAYLOAD_KEY_MINERS;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.PreDestroy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

import rafalgierusz.toptal.blockchain.block.Block;
import rafalgierusz.toptal.blockchain.block.GenesisBlock;
import rafalgierusz.toptal.blockchain.common.Payload;
import rafalgierusz.toptal.blockchain.transaction.Input;
import rafalgierusz.toptal.blockchain.transaction.Output;
import rafalgierusz.toptal.blockchain.transaction.Transaction;
import rafalgierusz.toptal.blockchain.wallet.GenesisWallet;
import rafalgierusz.toptal.blockchain.wallet.Wallet;

@Component
public class Chain {
	private static final Logger LOGGER = LoggerFactory.getLogger(Chain.class);

	public static final Long BLOCK_PERIOD = TimeUnit.MINUTES.toMillis(10);
	private static final int BLOCK_PERIOD_FORCE_COUNT = 10;

	private static final int NBITS = 0x1f00ffff;
	private static final BigInteger DIFFICULTY_TARGET = nBitsToBigInteger(NBITS);
	private static final Wallet GENESIS_WALLET = new GenesisWallet();
	private static final BigInteger REWARD = BigInteger.valueOf(100);

	private final Block GENESIS_BLOCK = new GenesisBlock();
	private final Set<String> ALLOWED_MINERS = Arrays
			.stream(GENESIS_BLOCK.getPayload().getData().get(PAYLOAD_KEY_MINERS).split("\\,"))
			.collect(Collectors.toSet());

	private List<Block> blocks = new CopyOnWriteArrayList<>();
	private Map<String, Block> hashToBlockMap = new ConcurrentHashMap<>();
	private Map<String, Transaction> hashToTransactionMap = new ConcurrentHashMap<>();

	private Map<String, Output> utxos = new LinkedHashMap<String, Output>();

	private Map<String, Transaction> unprocessedTransactions = new ConcurrentHashMap<>();
	private Map<String, Transaction> publishedTransactions = new ConcurrentHashMap<>();

	private ScheduledExecutorService transactionExecutor = Executors.newSingleThreadScheduledExecutor();
	private volatile Runnable currentTransactionExecutoTask;

	private volatile Block lastSyncBlock = GENESIS_BLOCK;

	@Autowired
	NodeManager nodeManager;

	public Chain() {
		blocks.add(GENESIS_BLOCK);
		hashToBlockMap.put(GENESIS_BLOCK.getHash(), GENESIS_BLOCK);

		Transaction genesisTransaction = GENESIS_BLOCK.getTransactions().get(0);
		hashToTransactionMap.put(genesisTransaction.getHash(), genesisTransaction);
		utxos.put(genesisTransaction.getOutputs().get(0).getHash(), genesisTransaction.getOutputs().get(0));

		publishUnprocessedTransactions();
	}

	public void publishUnprocessedTransactions() {
		synchronized (unprocessedTransactions) {
			for (Entry<String, Transaction> transactionEntry : unprocessedTransactions.entrySet()) {
				// move from unprocessed to published
				publishedTransactions.put(transactionEntry.getKey(), transactionEntry.getValue());
				unprocessedTransactions.remove(transactionEntry.getKey());

				// remove from published after BLOCK_PERIOD
				transactionExecutor.schedule(() -> {
					publishedTransactions.remove(transactionEntry.getKey());
				}, BLOCK_PERIOD, TimeUnit.MILLISECONDS);
			}
		}

		// schedule next period and update current task
		transactionExecutor.schedule(currentTransactionExecutoTask = () -> {
			if (currentTransactionExecutoTask != this) {
				// was replaced with other task, ignore this one
				return;
			}

			publishUnprocessedTransactions();
		}, BLOCK_PERIOD, TimeUnit.MILLISECONDS);
	}

	public void add(Block block) {
		add(block, true);
	}

	public synchronized void add(Block block, boolean broadcast) {
		// 1: check if miner is allowed
		// 2: verify block and difficulty target
		// 3: verify transactions and their inputs
		// 4: check if each transaction is still planned/valid to be processed

		// 1
		if (!ALLOWED_MINERS.contains(block.getPublicKey())
				&& !GENESIS_WALLET.getPublicKeySerialized().equals(block.getPublicKey())) {
			throw new IllegalArgumentException("Illegal miner");
		}

		// 2
		if (!block.verify(getCurrentBlock().getHash(), DIFFICULTY_TARGET)) {
			throw new IllegalArgumentException("Incorrect block");
		}

		// 3
		for (Transaction transaction : block.getTransactions()) {
			if (!transaction.verify() || !validateInputs(transaction)) {
				throw new IllegalArgumentException("Incorrect transaction");
			}

			// check if inputs amount equals outputs amount
			BigInteger inputsAmount = transaction.getInputs().stream().map(Input::getSourceOutputHash).map(utxos::get)
					.map(Output::getAmount).reduce(BigInteger.ZERO, BigInteger::add);
			BigInteger outputsAmount = transaction.getOutputs().stream().map(Output::getAmount).reduce(BigInteger.ZERO,
					BigInteger::add);

			if (!inputsAmount.equals(outputsAmount)) {
				throw new IllegalArgumentException("Incorrect input <-> output balance");
			}
		}

		synchronized (unprocessedTransactions) {
			if (broadcast && !GENESIS_WALLET.getPublicKeySerialized().equals(block.getPublicKey())) {
				// 4
				if (!publishedTransactions.keySet().stream().collect(Collectors.toSet()).containsAll(
						block.getTransactions().stream().map(Transaction::getHash).collect(Collectors.toSet()))) {
					throw new IllegalArgumentException("Transaction not published");
				}
			}

			if (broadcast) {
				if (!nodeManager.broadcast(block)) {
					LOGGER.info("Considered as incorrect by other node, will sync now: {}", block.getHash());

					// incorrect block, sync
					sync();

					return;
				}
			}

			blocks.add(block);
			hashToBlockMap.put(block.getHash(), block);

			block.getTransactions().forEach(transaction -> {
				processTransaction(transaction);
				hashToTransactionMap.put(transaction.getHash(), transaction);
				publishedTransactions.remove(transaction.getHash());
			});

			lastSyncBlock = block;

			if (broadcast && !GENESIS_WALLET.getPublicKeySerialized().equals(block.getPublicKey())) {
				rewardMiner(block.getPublicKey());
			}
		}
	}

	private void rewardMiner(String minerPublicKey) {
		Transaction rewardTransaction = crateTransaction(GENESIS_WALLET.getPublicKeySerialized(), minerPublicKey,
				REWARD, null).sign(GENESIS_WALLET);

		Payload rewardBlockPayload = new Payload(Map.of("type", "reward", "miner", minerPublicKey))
				.sign(GENESIS_WALLET);

		Block rewardBlock = new Block(getCurrentBlock().getHash());
		rewardBlock.addTransaction(rewardTransaction);
		rewardBlock.mine(GENESIS_WALLET, rewardBlockPayload, DIFFICULTY_TARGET);

		add(rewardBlock);
		// blocks.add(rewardBlock);
		// hashToBlockMap.put(rewardBlock.getHash(), rewardBlock);
		//
		// processTransaction(rewardTransaction);
		// hashToTransactionMap.put(rewardTransaction.getHash(), rewardTransaction);
	}

	public synchronized boolean validate() { // recreate the whole transaction history
		// to keep the order
		Map<String, Output> testUtxos = new LinkedHashMap<>();

		String previousBlockHash = null;

		for (Block block : blocks) {
			if (!block.verify(previousBlockHash, DIFFICULTY_TARGET)) {
				return false;
			}

			for (Transaction transaction : block.getTransactions()) {
				if (!transaction.verify()) {
					return false;
				}

				// do not process inputs for genesis block
				if (block.getPreviousHash() != null) {
					for (Input input : transaction.getInputs()) {
						if (testUtxos.remove(input.getSourceOutputHash()) == null) {
							return false;
						}
					}
				}

				for (Output output : transaction.getOutputs()) {
					if (testUtxos.put(output.getHash(), output) != null) {
						// terminate if output already exist
						return false;
					}
				}

				// first output always for recipient
				if (!transaction.getOutputs().get(0).getRecipientPublicKey()
						.equals(transaction.getRecipientPublicKey())) {
					return false;
				}

				// second output always for sender - unspent - if exist
				if (transaction.getOutputs().size() > 1 && !transaction.getOutputs().get(1).getRecipientPublicKey()
						.equals(transaction.getSenderPublicKey())) {
					return false;
				}
			}

			previousBlockHash = block.getHash();
		}

		return true;
	}

	public synchronized int getBlockCount() {
		return blocks.size();
	}

	public synchronized Block getBlock(String hash) {
		return hashToBlockMap.get(hash);
	}

	@JsonIgnore
	public synchronized Block getCurrentBlock() {
		return blocks.get(blocks.size() - 1);
	}

	public synchronized List<Block> getBlocks() {
		List<Block> result = new ArrayList<>(blocks);

		Collections.reverse(result);

		return result;
	}

	public synchronized Transaction getTransaction(String hash) {
		return hashToTransactionMap.get(hash);
	}

	public synchronized Map<String, Output> getUtxos() {
		return utxos;
	}

	public BigInteger getDifficultyTarget() {
		return DIFFICULTY_TARGET;
	}

	// ******************
	// * WALLET SECTION *
	// ******************

	private Stream<Output> collectOutputsForWallet(String publicKey) {
		return utxos.values().stream().filter(output -> publicKey.equals(output.getRecipientPublicKey()));
	}

	public synchronized BigInteger getBalance(String publicKey) {
		return collectOutputsForWallet(publicKey).map(Output::getAmount).reduce(BigInteger.ZERO, BigInteger::add);
	}

	// ***********************
	// * TRANSACTION SECTION *
	// ***********************

	public synchronized boolean verifyInput(Input input) {
		return utxos.containsKey(input.getSourceOutputHash());
	}

	public synchronized Transaction crateTransaction(String senderPublicKey, String recipientPublicKey,
			BigInteger amount, Payload payload) {
		// 1: check sender's balance - is it even possible?
		// 2: fill in inputs based on existing/previous outputs
		// 2.a: use only inputs/outputs that are really needed
		// 3: create transaction
		// 4: create regular output
		// 5: create unspent output - if applies
		// 6: return unsigned transaction

		// 1
		if (getBalance(senderPublicKey).compareTo(amount) < 0) {
			throw new IllegalStateException("Not enough funds");
		}

		List<Input> inputs = new ArrayList<>();

		// 2
		BigInteger outputSum = BigInteger.ZERO;
		for (Output output : collectOutputsForWallet(senderPublicKey).collect(Collectors.toList())) {
			outputSum = outputSum.add(output.getAmount());
			inputs.add(new Input(output.getHash()));

			// 2.a
			if (outputSum.compareTo(amount) >= 0) {
				break;
			}
		}

		// 3
		Transaction transaction = new Transaction(senderPublicKey, recipientPublicKey, amount, payload, inputs);

		// 4
		transaction.addOutput(new Output(recipientPublicKey, amount, transaction.getHash()));

		// 5
		if (outputSum.compareTo(amount) > 0) {
			transaction.addOutput(new Output(senderPublicKey, outputSum.subtract(amount), transaction.getHash()));
		}

		// 6
		return transaction;
	}

	// called only from synchronized methods
	private void processTransaction(Transaction transaction) {
		// collect inputs amount
		// BigInteger inputsAmount =
		// transaction.getInputs().stream().map(Input::getSourceOutputHash).map(utxos::get)
		// .map(Output::getAmount).reduce(BigInteger.ZERO, BigInteger::add);

		// process outputs
		// transaction.addOutput(
		// new Output(transaction.getRecipientPublicKey(), transaction.getAmount(),
		// transaction.getHash()));

		// BigInteger amountUnspent = inputsAmount.subtract(transaction.getAmount());
		// if (amountUnspent.signum() > 0) {
		// transaction.addOutput(new Output(transaction.getSenderPublicKey(),
		// amountUnspent, transaction.getHash()));
		// }

		// register unspent within the chain
		transaction.getOutputs().forEach(output -> utxos.put(output.getHash(), output));

		// clear spent from the chain
		transaction.getInputs().forEach(input -> utxos.remove(input.getSourceOutputHash()));
	}

	private boolean validateInputs(Transaction transaction) {
		return transaction.getInputs().stream().map(Input::getSourceOutputHash).allMatch(utxos::containsKey);
	}

	public boolean addUnprocessedTransaction(Transaction transaction) {
		// 1: verify transaction
		// 2: check if all the inputs have matching outputs
		// 3: check duplicate sender sender

		// 1
		if (!transaction.verify()) {
			throw new IllegalArgumentException("Incorrect transaction");
		}

		synchronized (unprocessedTransactions) {
			// 2
			if (!validateInputs(transaction)) {
				throw new IllegalArgumentException("Deprecated set of inputs");
			}

			// 3
			Set<String> senders = unprocessedTransactions.values().stream().map(Transaction::getSenderPublicKey)
					.collect(Collectors.toSet());
			senders.addAll(publishedTransactions.values().stream().map(Transaction::getSenderPublicKey)
					.collect(Collectors.toSet()));

			if (senders.contains(transaction.getSenderPublicKey())) {
				throw new IllegalArgumentException("Only one transaction per sender allowed");
			}

			unprocessedTransactions.put(transaction.getHash(), transaction);

			if (unprocessedTransactions.size() >= BLOCK_PERIOD_FORCE_COUNT) {
				publishUnprocessedTransactions();
			}

			return true;
		}
	}

	public synchronized boolean processBlockFromOtherNode(Block block) {
		try {
			add(block, false);
			return true;
		} catch (Exception e) {
			sync();
			return false;
		}
	}

	private void sync() {
		while (!getCurrentBlock().getHash().equals(lastSyncBlock.getHash())) {
			hashToBlockMap.remove(getCurrentBlock().getHash());
			blocks.remove(blocks.size() - 1);
		}

		unprocessedTransactions.clear();
		publishedTransactions.clear();

		rebuildUTXO();
	}

	private void rebuildUTXO() {
		utxos.clear();

		for (Block block : blocks) {
			for (Transaction transaction : block.getTransactions()) {
				// do not process inputs for genesis block
				if (block.getPreviousHash() != null) {
					transaction.getInputs().forEach(input -> {
						utxos.remove(input.getSourceOutputHash());
					});
				}

				transaction.getOutputs().forEach(output -> {
					utxos.put(output.getHash(), output);
				});
			}
		}
	}

	public Map<String, Transaction> getPublishedTransactions() {
		return publishedTransactions;
	}

	@PreDestroy
	public void dispose() {
		transactionExecutor.shutdownNow();
	}
}
