package rafalgierusz.toptal.blockchain.chain;

import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;

import rafalgierusz.toptal.blockchain.block.Block;

@Component
public class NodeManager {
	private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

	private static final Logger LOGGER = LoggerFactory.getLogger(NodeManager.class);

	@Resource
	public Environment env;

	private WebResource node;

	@PostConstruct
	public void init() throws MalformedURLException, UnknownHostException {
		String currentNodeIP = InetAddress.getLocalHost().getHostAddress();
		int currentNodePort = env.getProperty("server.port", Integer.class);

		String[] stringNodes = env.getProperty("nodes").split(",");

		if (stringNodes.length != 2) {
			throw new IllegalArgumentException("Incorrect number of nodes");
		}

		for (String node : stringNodes) {
			URL nodeURL = new URL(node + "/node-block-check");

			if (currentNodeIP.equals(nodeURL.getHost()) && currentNodePort == nodeURL.getPort()) {
				// current node
				continue;
			}

			ClientConfig clientConfig = new DefaultClientConfig();
			clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);

			Client client = Client.create(clientConfig);

			this.node = client.resource(nodeURL.toString());
		}

		if (this.node == null) {
			throw new IllegalStateException("Pair node not initialized");
		}
	}

	public boolean broadcast(Block block) {
		LOGGER.info("Broadcasting block: " + block.getHash());

		try {
			ClientResponse response = node.accept("application/json").type("application/json")
					.post(ClientResponse.class, block);

			if (response.getStatus() != 200) {
				// should not happen, assume rejection
				return false;
			}

			try {
				String stringResponse = response.getEntity(String.class);
				return StringUtils.isEmpty(stringResponse) ? false
						: OBJECT_MAPPER.readValue(stringResponse, Boolean.class);
			} catch (Exception e) {
				// should not happen, assume rejection
				LOGGER.warn("Broadcasting response: rejection");
				return false;
			}
		} catch (Exception e) {
			LOGGER.warn("Broadcast exception: {}", e.getMessage());
			LOGGER.warn("Broadcasting response: rejection");

			// assume approval
			return false;
		}
	}
}
