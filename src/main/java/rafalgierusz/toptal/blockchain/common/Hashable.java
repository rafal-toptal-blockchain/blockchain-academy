package rafalgierusz.toptal.blockchain.common;

import static rafalgierusz.toptal.blockchain.Utils.hash;

public interface Hashable {
	public String getHash();

	default public String calculateHash() {
		return hash(prepareContentToHash());
	}

	default public boolean verifyHash() {
		return getHash() != null && getHash().equals(calculateHash());
	}

	public String prepareContentToHash();
}
