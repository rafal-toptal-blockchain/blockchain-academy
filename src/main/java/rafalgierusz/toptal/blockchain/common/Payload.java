package rafalgierusz.toptal.blockchain.common;

import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class Payload extends Signable<Payload> {
	// to keep order within the data;
	private NavigableMap<String, String> data;

	public Payload() {
	}

	public Payload(Map<String, String> data) {
		if (data == null || data.isEmpty()) {
			throw new IllegalArgumentException("Empty data");
		}

		this.data = data instanceof NavigableMap ? (NavigableMap<String, String>) data : new TreeMap<>(data);
	}

	// to reflect the order within the data
	public NavigableMap<String, String> getData() {
		return new TreeMap<>(data);
	}

	@Override
	protected String prepareContentToSign() {
		return data.entrySet().stream().map(dataEntry -> dataEntry.getKey() + "=" + dataEntry.getValue())
				.collect(Collectors.joining(","));
	}
}
