package rafalgierusz.toptal.blockchain.common;

import static rafalgierusz.toptal.blockchain.Utils.deserializePublicKey;

import rafalgierusz.toptal.blockchain.Utils;
import rafalgierusz.toptal.blockchain.wallet.Wallet;

public abstract class Signable<T extends Signable<T>> {
	private String publicKey;
	private String signature;

	public T sign(Wallet wallet) {
		if (wallet == null || wallet.getPrivateKey() == null || wallet.getPublicKey() == null) {
			throw new IllegalArgumentException("Incorrect keys");
		}

		this.publicKey = wallet.getPublicKeySerialized();
		this.signature = Utils.sign(wallet.getPrivateKey(), prepareContentToSign());

		@SuppressWarnings("unchecked")
		T thisInstance = (T) this;

		return thisInstance;
	}

	public T initSignature(String publicKey, String signature) {
		this.publicKey = publicKey;
		this.signature = signature;

		@SuppressWarnings("unchecked")
		T thisInstance = (T) this;

		return thisInstance;
	}

	public boolean verifySignature() {
		return publicKey != null && signature != null
				&& Utils.verifySignature(deserializePublicKey(publicKey), prepareContentToSign(), signature);
	}

	protected abstract String prepareContentToSign();

	public String getPublicKey() {
		return publicKey;
	}

	public String getSignature() {
		return signature;
	}

	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}
}
