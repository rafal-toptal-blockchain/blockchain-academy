package rafalgierusz.toptal.blockchain.transaction;

import java.util.Map;

import rafalgierusz.toptal.blockchain.common.Payload;
import rafalgierusz.toptal.blockchain.wallet.GenesisWallet;

public class GenesisTransacion extends Transaction {
	private static final String PUBLIC_KEY = new GenesisWallet().getPublicKeySerialized();
	private static final String SIGNATURE = "30460221009c4bfdf0d93cad036a5c1563850ea8e02348bb6379cf3827f5d1cdd5c88162a0022100a4ad52053452d3d6921b0c36d0040aadbdb6a938437d17dd69713d6bea8fb35d";
	private static final String PAYLOAD_SIGNATURE = "3045022100ef6bd10fdf5eda7a844c5f47628a8a91c60f3b7688dd9788c044b685ad91cf7102207dc9499ac07584ea8494ffc564e3172a4bbd29dc13a69a14cce7d4ed2d9e78c2";

	public GenesisTransacion() {
		super(PUBLIC_KEY,
				new Payload(Map.of("message", "Genesis Transaction")).initSignature(PUBLIC_KEY, PAYLOAD_SIGNATURE));

		initSignature(PUBLIC_KEY, SIGNATURE);
	}

//	 public GenesisTransacion(String privateKey) {
//	 super(PUBLIC_KEY, new Payload(Map.of("message", "Genesis Transaction")).sign(new Wallet(privateKey, PUBLIC_KEY)));
//	
//	 sign(new Wallet(privateKey, PUBLIC_KEY));
//	 }

	@Override
	public boolean verify() {
		return verifySignature();
	}
}
