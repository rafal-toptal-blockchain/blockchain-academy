package rafalgierusz.toptal.blockchain.transaction;

import rafalgierusz.toptal.blockchain.common.Hashable;

public class Input implements Hashable {
	private String hash;

	private String sourceOutputHash;

	public Input() {
	}

	public Input(String sourceOutputHash) {
		this.sourceOutputHash = sourceOutputHash;

		this.hash = calculateHash();
	}

	@Override
	public String getHash() {
		return hash;
	}

	public String getSourceOutputHash() {
		return sourceOutputHash;
	}

	@Override
	public String prepareContentToHash() {
		return sourceOutputHash;
	}
}
