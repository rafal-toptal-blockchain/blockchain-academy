package rafalgierusz.toptal.blockchain.transaction;

import java.math.BigInteger;

import rafalgierusz.toptal.blockchain.common.Hashable;

public final class Output implements Hashable {
	private String hash;
	private String recipientPublicKey;
	private BigInteger amount;
	private String transactionHash;

	public Output() {
	}
	
	public Output(String recipientPublicKey, BigInteger amount, String transactionHash) {
		this.recipientPublicKey = recipientPublicKey;
		this.amount = amount;
		this.transactionHash = transactionHash;

		this.hash = calculateHash();
	}

	public String getHash() {
		return hash;
	}

	public String getRecipientPublicKey() {
		return recipientPublicKey;
	}

	public BigInteger getAmount() {
		return amount;
	}

	public String getTransactionHash() {
		return transactionHash;
	}

	public String prepareContentToHash() {
		return new StringBuilder(recipientPublicKey).append(",").append(amount).append(",").append(transactionHash)
				.toString();
	}
}
