package rafalgierusz.toptal.blockchain.transaction;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import rafalgierusz.toptal.blockchain.common.Hashable;
import rafalgierusz.toptal.blockchain.common.Payload;
import rafalgierusz.toptal.blockchain.common.Signable;
import rafalgierusz.toptal.blockchain.wallet.Wallet;

public class Transaction extends Signable<Transaction> implements Hashable {
	private static final AtomicInteger GLOBAL_INDEX = new AtomicInteger();

	private String hash;

	private int index;

	private String senderPublicKey;
	private String recipientPublicKey;

	private BigInteger amount;
	private Payload payload;

	private List<Input> inputs = new ArrayList<>();
	private List<Output> outputs = new ArrayList<>();

	public Transaction() {
	}

	public Transaction(String senderPublicKey, String recipientPublicKey, BigInteger amount, Payload payload,
			List<Input> inputs) {
		index = GLOBAL_INDEX.getAndIncrement();

		if (senderPublicKey == null || recipientPublicKey == null) {
			throw new IllegalArgumentException("Incorrect keys");
		}

		if (amount == null || amount.compareTo(BigInteger.ZERO) <= 0) {
			throw new IllegalArgumentException("Incorrect keys");
		}

		if (inputs == null || inputs.isEmpty()) {
			throw new IllegalArgumentException("Incorrect inputs");
		}

		this.senderPublicKey = senderPublicKey;
		this.recipientPublicKey = recipientPublicKey;
		this.amount = amount;
		this.inputs = inputs;

		if (payload != null) {
			if (!payload.verifySignature() || !payload.getPublicKey().equals(senderPublicKey)) {
				throw new IllegalArgumentException("Incorrect payload or payload signer");
			}

			this.payload = payload;
		}

		hash = calculateHash();
	}

	// implemented only to be used with Genesis Transaction
	protected Transaction(String publicKey, Payload payload) {
		index = GLOBAL_INDEX.getAndIncrement();

		this.senderPublicKey = publicKey;
		this.recipientPublicKey = publicKey;
		this.amount = BigInteger.valueOf(1_000_000_000);
		this.payload = payload;

		hash = calculateHash();

		addOutput(new Output(publicKey, amount, hash));
	}

	@Override
	public String prepareContentToHash() {
		StringBuilder builder = new StringBuilder(index).append(",").append(senderPublicKey).append(",")
				.append(recipientPublicKey).append(",").append(amount);

		if (payload != null) {
			builder.append(",").append(payload.getSignature()).toString();
		}

		return builder.append(",")
				.append(inputs.stream().map(input -> input.getHash()).collect(Collectors.joining(","))).toString();
	}

	@Override
	protected String prepareContentToSign() {
		return hash + "," + prepareContentToHash() + ", "
				+ outputs.stream().map(Output::getHash).collect(Collectors.joining(","));
	}

	@Override
	public Transaction sign(Wallet wallet) {
		if (wallet == null || wallet.getPrivateKey() == null || wallet.getPublicKey() == null) {
			throw new IllegalArgumentException("Incorrect keys");
		}

		if (!wallet.getPublicKeySerialized().equals(senderPublicKey)) {
			throw new IllegalArgumentException("Can be signed only by sender");
		}

		return super.sign(wallet);
	}

	@Override
	public String getHash() {
		return hash;
	}

	/**
	 * Checked: hash, signature, inputs, output and payload
	 */
	public boolean verify() {
		return verifyHash() && verifySignature() && senderPublicKey.equals(getPublicKey())
				&& (payload == null || (payload.verifySignature() && payload.getPublicKey().equals(getPublicKey())))
				&& inputs.stream().allMatch(Input::verifyHash) && outputs.stream().allMatch(Output::verifyHash)
				&& outputs.stream().map(Output::getTransactionHash).allMatch(hash::equals);
	}

	public BigInteger getAmount() {
		return amount;
	}

	public List<Input> getInputs() {
		return inputs;
	}

	public void addOutput(Output output) {
		if (outputs.size() >= 2) {
			throw new IllegalArgumentException("Only two outputs per transaction allowed");
		}

		if (!hash.equals(output.getTransactionHash())) {
			throw new IllegalArgumentException("Illegral output");
		}

		outputs.add(output);
	}

	public List<Output> getOutputs() {
		return outputs;
	}

	public String getSenderPublicKey() {
		return senderPublicKey;
	}

	public String getRecipientPublicKey() {
		return recipientPublicKey;
	}

	public Payload getPayload() {
		return payload;
	}
}
