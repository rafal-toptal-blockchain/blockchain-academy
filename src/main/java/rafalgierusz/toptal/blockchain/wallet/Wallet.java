package rafalgierusz.toptal.blockchain.wallet;

import static rafalgierusz.toptal.blockchain.Utils.createKeyPair;
import static rafalgierusz.toptal.blockchain.Utils.deserializePrivateKey;
import static rafalgierusz.toptal.blockchain.Utils.deserializePublicKey;
import static rafalgierusz.toptal.blockchain.Utils.serializeKey;

import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;

public class Wallet {
	private final PrivateKey privateKey;
	private final PublicKey publicKey;

	private final String privateKeySerialized;
	private final String publicKeySerialized;

	public Wallet() {
		KeyPair keyPair = createKeyPair();

		privateKey = keyPair.getPrivate();
		publicKey = keyPair.getPublic();

		privateKeySerialized = serializeKey(privateKey);
		publicKeySerialized = serializeKey(publicKey);
	}

	// created only for Genesis Wallet
	protected Wallet(String privateKeySerialized, String publicKeySerialized) {
		this.privateKey = deserializePrivateKey(privateKeySerialized);
		this.publicKey = deserializePublicKey(publicKeySerialized);

		this.privateKeySerialized = privateKeySerialized;
		this.publicKeySerialized = publicKeySerialized;
	}

	public PrivateKey getPrivateKey() {
		return privateKey;
	}

	public PublicKey getPublicKey() {
		return publicKey;
	}

	public String getPrivateKeySerialized() {
		return privateKeySerialized;
	}

	public String getPublicKeySerialized() {
		return publicKeySerialized;
	}
}
