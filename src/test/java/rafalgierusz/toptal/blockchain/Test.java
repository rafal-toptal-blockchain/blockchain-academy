package rafalgierusz.toptal.blockchain;

import java.math.BigInteger;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import rafalgierusz.toptal.blockchain.block.Block;
import rafalgierusz.toptal.blockchain.chain.Chain;
import rafalgierusz.toptal.blockchain.common.Payload;
import rafalgierusz.toptal.blockchain.transaction.Transaction;
import rafalgierusz.toptal.blockchain.wallet.GenesisWallet;
import rafalgierusz.toptal.blockchain.wallet.Wallet;
import rafalgierusz.toptal.blockchain.wallet.WalletMiner1;
import rafalgierusz.toptal.blockchain.wallet.WalletMiner2;
import rafalgierusz.toptal.blockchain.wallet.WalletMiner3;
import rafalgierusz.toptal.blockchain.wallet.WalletMiner5;

/**
 * This test is not functional at this moment 
 */

@SuppressWarnings("unused")
public class Test {
	public static void main(String[] args) throws JsonProcessingException {
		test1();
	}

	private static void test1() throws JsonProcessingException {
		Chain chain = new Chain();
		Wallet genesisWallet = new GenesisWallet();

		Block genesisBlock = chain.getBlock("0000e2b1050bc20dd4ad63c96099e2068ab7be2808bed488d3b674adeb897c95");
		System.out.println(genesisBlock.verify(null, chain.getDifficultyTarget()) ? "Genesis block is valid"
				: "Genesis block is invalid");
		System.out.println(new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(genesisBlock));

		Wallet walletMiner1 = new WalletMiner1();
		Wallet walletMiner2 = new WalletMiner2();

		System.out.println("=============================================================");

		for (int i = 0; i < 5; i++) {
			Transaction transaction = chain.crateTransaction(genesisWallet.getPublicKeySerialized(),
					walletMiner2.getPublicKeySerialized(), BigInteger.ONE, null).sign(genesisWallet);

			chain.addUnprocessedTransaction(transaction);
			chain.publishUnprocessedTransactions();

			Block block = new Block(chain.getCurrentBlock().getHash());
			block.addTransaction(transaction);

			block = block.mine( //
					walletMiner1, //
					new Payload(Map.of("" + (char) ('a' + i), "" + ((char) ('a' + i)) + ((char) ('a' + i)))) //
							.sign(walletMiner1), //
					chain.getDifficultyTarget() //
			);

			chain.add(block);
		}

		System.out.println(chain.validate() ? "Chain is valid" : "Chain is invalid");
		System.out.println(new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(chain));

		System.out.println("=============================================================");

		System.out.println("Genesis wallet funds: " + chain.getBalance(genesisWallet.getPublicKeySerialized()));
		System.out.println("Miner1 funds: " + chain.getBalance(walletMiner1.getPublicKeySerialized()));
		System.out.println("Miner2 funds: " + chain.getBalance(walletMiner2.getPublicKeySerialized()));

		System.exit(0);
	}

	private static void test2() throws JsonProcessingException {
		Chain chain = new Chain();

		Wallet genesisWallet = new GenesisWallet();
		Wallet walletMiner1 = new WalletMiner1();
		Wallet walletMiner2 = new WalletMiner2();
		Wallet walletMiner3 = new WalletMiner3();
		Wallet walletMiner5 = new WalletMiner5();

		//
		Payload transaction1Payload = new Payload(Map.of("tx1Key", "tx1Value")).sign(genesisWallet);
		Transaction transaction1 = chain.crateTransaction(genesisWallet.getPublicKeySerialized(),
				walletMiner1.getPublicKeySerialized(), BigInteger.valueOf(223), transaction1Payload)
				.sign(genesisWallet);

		chain.addUnprocessedTransaction(transaction1);
		chain.publishUnprocessedTransactions();

		Block block1 = new Block(chain.getCurrentBlock().getHash());
		block1.addTransaction(transaction1);

		Payload block1Payload = new Payload(Map.of("block1Key", "block1Value")).sign(walletMiner5);
		block1.mine(walletMiner5, block1Payload, chain.getDifficultyTarget());

		chain.add(block1);

		//
		Payload transaction2Payload = new Payload(Map.of("tx2Key", "tx2Value")).sign(genesisWallet);
		Transaction transaction2 = chain.crateTransaction(genesisWallet.getPublicKeySerialized(),
				walletMiner2.getPublicKeySerialized(), BigInteger.valueOf(222), transaction2Payload)
				.sign(genesisWallet);
		Transaction transaction3 = chain.crateTransaction(walletMiner1.getPublicKeySerialized(),
				walletMiner3.getPublicKeySerialized(), BigInteger.valueOf(3), null).sign(walletMiner1);

		chain.addUnprocessedTransaction(transaction2);
		chain.addUnprocessedTransaction(transaction3);
		chain.publishUnprocessedTransactions();

		Block block2 = new Block(chain.getCurrentBlock().getHash());
		block2.addTransaction(transaction2);
		block2.addTransaction(transaction3);

		Payload block2Payload = new Payload(Map.of("block2Key", "block2Value")).sign(walletMiner5);
		block2.mine(walletMiner5, block2Payload, chain.getDifficultyTarget());

		chain.add(block2);

		System.out.println(chain.validate() ? "Chain is valid" : "Chain is invalid");
		System.out.println(new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(chain));

		System.out.println("=============================================================");

		System.out.println("Genesis wallet funds: " + chain.getBalance(genesisWallet.getPublicKeySerialized()));
		System.out.println("Miner 1 wallet funds: " + chain.getBalance(walletMiner1.getPublicKeySerialized()));
		System.out.println("Miner 2 wallet funds: " + chain.getBalance(walletMiner2.getPublicKeySerialized()));
		System.out.println("Miner 3 wallet funds: " + chain.getBalance(walletMiner3.getPublicKeySerialized()));
		System.out.println("Miner 5 wallet funds: " + chain.getBalance(walletMiner5.getPublicKeySerialized()));

		System.exit(0);
	}
}
