package rafalgierusz.toptal.blockchain;

import java.io.IOException;
import java.math.BigInteger;
import java.util.Map;

import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import rafalgierusz.toptal.blockchain.block.Block;
import rafalgierusz.toptal.blockchain.common.Payload;
import rafalgierusz.toptal.blockchain.transaction.Transaction;
import rafalgierusz.toptal.blockchain.wallet.GenesisWallet;
import rafalgierusz.toptal.blockchain.wallet.Wallet;
import rafalgierusz.toptal.blockchain.wallet.WalletMiner1;
import rafalgierusz.toptal.blockchain.wallet.WalletMiner2;
import rafalgierusz.toptal.blockchain.wallet.WalletMiner3;
import rafalgierusz.toptal.blockchain.wallet.WalletMiner5;

@SuppressWarnings("unused")
public class TestJson {
	private static Runner runner;

	public static void main(String[] args) throws Exception {
		try (ConfigurableApplicationContext ctx = SpringApplication.run(Runner.class, args)) {
			runner = ctx.getBean(Runner.class);

			test3();
		}
	}

	private static void test1() throws IOException {
		Wallet genesisWallet = new GenesisWallet();
		Wallet walletMiner1 = new WalletMiner1();
		Wallet walletMiner2 = new WalletMiner2();

		Block genesisBlock = runner.getBlock("0000e2b1050bc20dd4ad63c96099e2068ab7be2808bed488d3b674adeb897c95");
		BigInteger difficultyTarget = runner.getDifficultyTarget();

		System.out.println(
				genesisBlock.verify(null, difficultyTarget) ? "Genesis block is valid" : "Genesis block is invalid");
		System.out.println(new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(genesisBlock));

		System.out.println("=============================================================");

		for (int i = 0; i < 5; i++) {
			Transaction transaction = runner.createTransaction(genesisWallet.getPublicKeySerialized(),
					walletMiner2.getPublicKeySerialized(), BigInteger.ONE, null).sign(genesisWallet);

			runner.submitTransaction(transaction);
			runner.publish();

			Block block = new Block(runner.getCurrentBlock().getHash());
			block.addTransaction(transaction);

			block = block.mine( //
					walletMiner1, //
					new Payload(Map.of("" + (char) ('a' + i), "" + ((char) ('a' + i)) + ((char) ('a' + i)))) //
							.sign(walletMiner1), //
					runner.getDifficultyTarget() //
			);

			runner.addBlock(block);
		}

		System.out.println(runner.isValid() ? "Chain is valid" : "Chain is invalid");

		System.out.println("=============================================================");

		System.out.println("Genesis wallet funds: " + runner.getBalance(genesisWallet.getPublicKeySerialized()));
		System.out.println("Miner1 funds: " + runner.getBalance(walletMiner1.getPublicKeySerialized()));
		System.out.println("Miner2 funds: " + runner.getBalance(walletMiner2.getPublicKeySerialized()));
	}

	private static void test2() throws IOException {
		Wallet genesisWallet = new GenesisWallet();
		Wallet walletMiner1 = new WalletMiner1();
		Wallet walletMiner2 = new WalletMiner2();
		Wallet walletMiner3 = new WalletMiner3();
		Wallet walletMiner5 = new WalletMiner5();

		//
		Payload transaction1Payload = new Payload(Map.of("tx1Key", "tx1Value")).sign(genesisWallet);
		Transaction transaction1 = runner.createTransaction(genesisWallet.getPublicKeySerialized(),
				walletMiner1.getPublicKeySerialized(), BigInteger.valueOf(223), transaction1Payload)
				.sign(genesisWallet);

		runner.submitTransaction(transaction1);
		runner.publish();

		Block block1 = new Block(runner.getCurrentBlock().getHash());
		block1.addTransaction(transaction1);

		Payload block1Payload = new Payload(Map.of("block1Key", "block1Value")).sign(walletMiner5);
		block1.mine(walletMiner5, block1Payload, runner.getDifficultyTarget());

		runner.addBlock(block1);

		//
		Payload transaction2Payload = new Payload(Map.of("tx2Key", "tx2Value")).sign(genesisWallet);
		Transaction transaction2 = runner.createTransaction(genesisWallet.getPublicKeySerialized(),
				walletMiner2.getPublicKeySerialized(), BigInteger.valueOf(222), transaction2Payload)
				.sign(genesisWallet);
		Transaction transaction3 = runner.createTransaction(walletMiner1.getPublicKeySerialized(),
				walletMiner3.getPublicKeySerialized(), BigInteger.valueOf(3), null).sign(walletMiner1);

		runner.submitTransaction(transaction2);
		runner.submitTransaction(transaction3);
		runner.publish();

		Block block2 = new Block(runner.getCurrentBlock().getHash());
		block2.addTransaction(transaction2);
		block2.addTransaction(transaction3);

		Payload block2Payload = new Payload(Map.of("block2Key", "block2Value")).sign(walletMiner5);
		block2.mine(walletMiner5, block2Payload, runner.getDifficultyTarget());

		runner.addBlock(block2);

		System.out.println(runner.isValid() ? "Chain is valid" : "Chain is invalid");

		System.out.println("=============================================================");

		System.out.println("Genesis wallet funds: " + runner.getBalance(genesisWallet.getPublicKeySerialized()));
		System.out.println("Miner 1 wallet funds: " + runner.getBalance(walletMiner1.getPublicKeySerialized()));
		System.out.println("Miner 2 wallet funds: " + runner.getBalance(walletMiner2.getPublicKeySerialized()));
		System.out.println("Miner 3 wallet funds: " + runner.getBalance(walletMiner3.getPublicKeySerialized()));
		System.out.println("Miner 5 wallet funds: " + runner.getBalance(walletMiner5.getPublicKeySerialized()));
	}

	// not to be used directly
	private static void test3() throws Exception {
		Wallet genesisWallet = new GenesisWallet();
		Wallet walletMiner1 = new WalletMiner1();
		Wallet walletMiner2 = new WalletMiner2();
		Wallet walletMiner3 = new WalletMiner3();
		Wallet walletMiner5 = new WalletMiner5();

		//
		Payload transaction1Payload = new Payload(Map.of("tx1Key", "tx1Value")).sign(genesisWallet);
		Transaction transaction1 = runner.createTransaction(genesisWallet.getPublicKeySerialized(),
				walletMiner1.getPublicKeySerialized(), BigInteger.valueOf(223), transaction1Payload)
				.sign(genesisWallet);

		runner.submitTransaction(transaction1);
		runner.publish();

		Block block1 = new Block(runner.getCurrentBlock().getHash());
		block1.addTransaction(transaction1);

		Payload block1Payload = new Payload(Map.of("block1Key", "block1Value")).sign(walletMiner5);
		block1.mine(walletMiner5, block1Payload, runner.getDifficultyTarget());

		Thread.sleep(3000);

		runner.addBlock(block1);
		
		///////////////////
		Payload transaction2Payload = new Payload(Map.of("tx2Key", "tx2Value")).sign(genesisWallet);
		Transaction transaction2 = runner.createTransaction(genesisWallet.getPublicKeySerialized(),
				walletMiner2.getPublicKeySerialized(), BigInteger.valueOf(222), transaction2Payload)
				.sign(genesisWallet);
		Transaction transaction3 = runner.createTransaction(walletMiner1.getPublicKeySerialized(),
				walletMiner3.getPublicKeySerialized(), BigInteger.valueOf(3), null).sign(walletMiner1);

		runner.submitTransaction(transaction2);
		runner.submitTransaction(transaction3);
		runner.publish();

		Block block2 = new Block(runner.getCurrentBlock().getHash());
		block2.addTransaction(transaction2);
		block2.addTransaction(transaction3);

		Payload block2Payload = new Payload(Map.of("block2Key", "block2Value")).sign(walletMiner5);
		block2.mine(walletMiner5, block2Payload, runner.getDifficultyTarget());
		
		Thread.sleep(3000);

		runner.addBlock(block2);

		System.in.read();
	}
}
